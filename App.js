import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { LogBox, Platform, StatusBar, StyleSheet, View } from 'react-native';

import useCachedRs from '@src/hooks/useCachedRs';
import MainTabNavigator from '@src/navigation/MainTabNavigator';
import AnonymousNavigator from '@src/navigation/AnonymousNavigator';
import LinkingConfiguration from '@src/navigation/LinkingConfiguration';

const Stack = createStackNavigator();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
]);

export default (props) => {
  const isLoadingComplete = useCachedRs();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
        <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
          <NavigationContainer linking={LinkingConfiguration}>
            {/* <Stack.Navigator> */}
            <Stack.Navigator initialRouteName="AnonymousStack">
              <Stack.Screen name="AnonymousStack" component={AnonymousNavigator} />
              <Stack.Screen name="AppStack" component={MainTabNavigator} />
              {/* <Stack.Screen name="AppStack" component={MainTabNavigator} /> */}
            </Stack.Navigator>
          </NavigationContainer>
        </View>
    );
  }
}