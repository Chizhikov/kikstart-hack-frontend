import React from 'react';
import NativeColors from '@src/constants/NativeColors';

export default () => {
  return NativeColors;
}
