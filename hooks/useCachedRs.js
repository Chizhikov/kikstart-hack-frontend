import { Ionicons } from '@expo/vector-icons';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import * as React from 'react';

export default () => {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    const loadResourcesAndDataAsync = async () => {
      try {
        SplashScreen.preventAutoHideAsync();
        
        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,

          'brown-thin': require('@src/assets/fonts/Brown-Thin.ttf'),
          'brown-light': require('@src/assets/fonts/Brown-Light.ttf'),
          'brown-regular': require('@src/assets/fonts/Brown-Regular.ttf'),
          'brown-bold': require('@src/assets/fonts/Brown-Bold.ttf'),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}