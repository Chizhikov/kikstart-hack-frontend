import React, { useEffect } from 'react';
import { createStackNavigator, header } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import WelcomeScreen from '@src/screens/WelcomeScreen';
import CreateWallet from '@src/screens/CreateWallet';
import MnemonicPhraseScreen from '@src/screens/MnemonicPhraseScreen';
import RecoveryWallet from '@src/screens/RecoveryWallet';

const Stack = createStackNavigator();

export default LoginStack = ({ navigation, route }) => {
  useEffect(() => {
    navigation.setOptions({ header: getHeader(route), headerTitle: getHeaderTitle(route) });
  },[])

  return (
    <Stack.Navigator screenOptions={{gestureEnabled: false}}>
      <Stack.Screen name="Welcome" component={WelcomeScreen} />
      <Stack.Screen name="CreateWallet" component={CreateWallet} />
      <Stack.Screen name="MnemonicPhraseScreen" component={MnemonicPhraseScreen} />
      <Stack.Screen name="RecoveryWallet" component={RecoveryWallet} />
    </Stack.Navigator>
  );
}

const getRouteName = (route) => {
  return getFocusedRouteNameFromRoute(route) ?? 'Welcome';
}

const getHeader = (route) => {
  const routeName = getRouteName(route);

  switch (routeName) {
    case 'Welcome':
    case 'CreateWallet':
    case 'MnemonicPhraseScreen':
    case 'RecoveryWallet':
      return () => {};

    default:
      return header;
  }
}

const getHeaderTitle = (route) => {
  const routeName = getRouteName(route);

  switch (routeName) {
    case 'Welcome':
    case 'CreateWallet':
    case 'MnemonicPhraseScreen':
    case 'RecoveryWallet':
      return '';

    default: 
      return routeName;
  }
}