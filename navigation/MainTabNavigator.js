import React, { useEffect } from 'react';
import { Platform, Text } from 'react-native';
import { createBottomTabNavigator, header } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import Icon from '@src/components/Icon';
import Colors from '@src/constants/Colors';
import Layout from '@src/constants/Layout';

// Wallet Stack
import WalletScreen from '@src/screens/WalletScreen';
import SendScreen from '@src/screens/SendScreen';
import ReceiveScreen from '@src/screens/ReceiveScreen';

// PROFILE Stack
import ProfileScreen from '@src/screens/ProfileScreen';
import MyWallets from '@src/screens/MyWallets';
import RecoveryWallet from '@src/screens/RecoveryWallet';
import MnemonicPhraseScreen from '@src/screens/MnemonicPhraseScreen';

// TRANSACTIONS Stack
import TransactionsScreen from '@src/screens/TransactionsScreen';
import TransactionInfo from '@src/screens/TransactionInfo';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Wallet';
const HEIGHT_BOTTOM_BAR = 80;
const HEIGHT_BOTTOM_BAR_XS = 115;

const Stack = createStackNavigator();

const WalletStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Wallet" component={WalletScreen} />
      <Stack.Screen name="SendToken" component={SendScreen} />
      <Stack.Screen name="ReceiveToken" component={ReceiveScreen} />
      <Stack.Screen name="Transactions" component={TransactionsScreen} />
      <Stack.Screen name="TransactionInfo" component={TransactionInfo} />
    </Stack.Navigator>
  );
}

const ProfileStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="MyWallets" component={MyWallets} />
      <Stack.Screen name="RecoveryWallet" component={RecoveryWallet} />
      <Stack.Screen name="MnemonicPhraseScreen" component={MnemonicPhraseScreen} />
    </Stack.Navigator>
  );
}

const SendStack = () => {
  return (
    <Stack.Navigator initialRouteName='Send'>
      <Stack.Screen name="Send" component={SendScreen} />
    </Stack.Navigator>
  );
}

export default MainTabNavigator = ({ navigation, route }) => {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  useEffect(() => {
    navigation.setOptions({ header: getHeader(route), headerTitle: getHeaderTitle(route) });
  },[])
  

  const tabBarOptions = {
    keyboardHidesTabBar: true,
    activeTintColor: Colors.sunBlue,
    // activeLabelColor: Colors.lightBlue,
    activeBackgroundColor: Colors.lightBlue,
    inactiveTintColor: Colors.gray2,
    style: {
      height: HEIGHT_BOTTOM_BAR,
    },
    showLabel: false,
    // tabStyle: {
    //   margin: 11,
    //   padding: 7,
    //   borderRadius: 3
    // },
    labelStyle: {
      fontSize: 12
    }
  }

  return (
    <BottomTab.Navigator 
      initialRouteName={INITIAL_ROUTE_NAME}
      tabBarOptions={tabBarOptions}
    >
      <BottomTab.Screen
        name="Wallet"
        component={WalletStack}
        options={{
          // title: "Wallet",
          tabBarIcon: ({ color }) => <Icon color={color} width={20} icon={require('@src/assets/images/home-tab.png')} />,
        }}
      />
      <BottomTab.Screen
        name="Send"
        component={SendStack}
        options={{
          title: "Send",
          tabBarIcon: ({ color, focused }) => (
            <Icon
              color={focused ? color : Colors.baliHai}
              width={20}
              icon={require('@src/assets/images/send-tab.png')}
            />
          ),
          // tabBarLabel: ({ color, focused }) => (
          //   <Text style={[
          //     tabBarOptions.labelStyle,
          //     !focused ? { color: Colors.baliHai } : { color }
          //   ]}>{ ScreenTitles.MAIN.Noah }</Text>
          // ),
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          title:  "Profile",
          tabBarIcon: ({ color }) => <Icon color={color} width={20} icon={require('@src/assets/images/user-tab.png')} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

const getRouteName = (route) => {
  return getFocusedRouteNameFromRoute(route) ?? 'Wallet';
}

const getHeader = (route) => {
  const routeName = getRouteName(route);

  switch (routeName) {
    case 'Wallet':
    case 'Send':
    case 'Profile':
      return () => {};

    default:
      return header;
  }
}

const getHeaderTitle = (route) => {
  const routeName = getRouteName(route);

  switch (routeName) {
    case 'Wallet':
    case 'Send':
    case 'Profile':
      return '';

    default: 
      return routeName;
  }
}
