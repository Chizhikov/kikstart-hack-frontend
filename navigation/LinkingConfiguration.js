import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      AnonymousStack: {
        initialRouteName: 'Welcome',
        // screens: {
        //   ApproveCode: 'approve-code',
        // },
      },
    },
  },
};
