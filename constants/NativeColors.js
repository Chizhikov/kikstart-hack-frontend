import Colors from '@src/constants/Colors';

export default {
  backgroundScreen: Colors.lightBlue,
  backgroundTabBar: Colors.white,
  backgroundSelectTabBar: Colors.gold,
  backgroundEdit: Colors.orange,

  backgroundButtonApply: Colors.sunBlue,
  backgroundButtonDecline: Colors.lightBlue,

  textButtonApply: Colors.white,
  textButtonDecline: Colors.sunBlue,

  borderButton: Colors.sunBlue,


  textButton: Colors.black,

  textActiveButton: Colors.white,
  textActiveTabBar: Colors.white,

  // textMain: Colors.rhino,
  // textAdditional: Colors.baliHai,
  // textSecure: Colors.ghost,
    
  backgroundInput: Colors.porcelain,
  text: Colors.black,
  textPlaceholderInput: Colors.blue,
  textInput: Colors.blue,

  backgroundIcon: Colors.backGray,

  textSuccess: Colors.green,
  textDecline: Colors.red,

  backgroundCopy: Colors.white,
  borderCopy: Colors.gray,

  text: Colors.darkBlue,
  textAdditional_Blue: Colors.blue,
  textAdditional_White: Colors.white,

  activityIndicatorColor: Colors.sunBlue,
}
