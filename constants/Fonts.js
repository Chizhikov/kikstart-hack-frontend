export default {
  thin: 'brown-thin',
  light: 'brown-light',
  regular: 'brown-regular',
  bold: 'brown-bold'
};