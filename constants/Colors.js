const black = '#000000';
const white = '#ffffff';
// const blue = 'blue'; 
const red = '#FA6400';
const orange = '#F38F38';
const gold = '#FBB000';
const green = '#21D000';
const gray = '#E7E7E7';
const backGray = '#E1E2F2';
const darkBlue = '#334D6E';
// const blue = 'rgba(51, 77, 110, 0.8)';
// const blue = 'rgba(67, 101, 134, 0.5)'; 
const blue = '#A1B2C2'
const lightBlue = 'rgba(224, 236, 248, 0.5)';
const sunBlue = '#2088EC';
const gray2 = '#B0BCC9';

export default {
  black,
  white,
  blue,
  red,
  orange,
  green,
  gray,
  gold,
  backGray,
  darkBlue,
  lightBlue,
  sunBlue,
};