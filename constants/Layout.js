import { Dimensions } from 'react-native';
import Constants from 'expo-constants'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  window: {
    width,
    height,
  },
  isExtraDevice: height > 750,
  statusBarHeight: Constants.statusBarHeight,
}

//^([0-9]\,|.)?[0-9]*$/
export const PATTERN_NUMBER = /^([0-9]+\.)?[0-9]*$/
export const PATTERN_NUMBER_IOS = /^[0-9]+((\,)[0-9]+)?$/
export const PATTERN_NUMBER_ANDROID = /^[0-9]+((\.)[0-9]+)?$/
// export const PATTERN_NUMBER = /^[0-9]+(\.[0-9]+)?$/