import * as React from 'react';

import Layout from '@src/components/Layout';

import Main from './Main';
import NavigationBar from '@src/components/NavigationBar';
import { SafeAreaView } from 'react-native';

export default CreateWallet = () => {
  return (
    <Layout backgroundColor='#fff'>
      <SafeAreaView style={{flex: 1}}>
        {/* <NavigationBar /> */}
        <Main />
      </SafeAreaView>
    </Layout>
  );
}