import React, { useState, useCallback, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, View, Text, ImageBackground } from 'react-native';
import lodash from 'lodash';

import useColors from '@src/hooks/useColors';
import Button from '@src/components/Button';
import Input from '@src/components/Input';
import Fonts from '@src/constants/Fonts';

import { createMnemomic, getWalletByMnemonic, importAddress } from '@src/api';
import { setStorage, getStorage, WALLETS } from '@src/store/system';
import Icon from '@src/components/Icon';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 16,
      flexDirection: 'column',
      paddingBottom: 20,
      paddingTop: 20,
      justifyContent: 'space-between'
    },
    containerButtonApply: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      backgroundColor: elementColors.backgroundButtonApply,
    },
    containerButtonDecline: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      borderWidth: 0,
    },
    textButtonApply: {
      fontSize: 18,
      marginTop: 5,
      color: elementColors.textButtonApply
    },
    textButtonDecline: {
      fontSize: 16,
      marginTop: 5,
      color: elementColors.textButtonDecline
    },
    containerTitle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    containerButtons: {

    },
    title: {
      textAlign: 'center',
      fontFamily: Fonts.bold,
      color: elementColors.text,
      fontSize: 30
    },
    title: {
      textAlign: 'center',
      fontFamily: Fonts.bold,
      color: elementColors.text,
      fontSize: 30
    },
  })
);

export default Main = () => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});
  const [name, setName] = useState('')

  const onChangeName = (text) => {
    setName(text)
  }

  const createNewWallet = async () => {
    const mnemonic = await createMnemomic();
    const wallet = await getWalletByMnemonic(mnemonic);

    if (wallet.privateKey) {
      const wallets = await getStorage(WALLETS) || [];

      if (!lodash.find(wallets, { privateKey: wallet.privateKey })) {
        await setStorage(WALLETS, [
          ...wallets,
          { address: wallet.address, privateKey: wallet.privateKey, publicKey: wallet.publicKey, mnemonic, name }
        ]);
      }

      await importAddress(wallet.address);

      navigation.navigate('AppStack')
    }
  }

  return (
    <ImageBackground
      resizeMode="contain"
      source={require("@src/assets/images/dashboard.png")}
      style={styles.container}
    >
      <Text style={styles.title}> Protect Your Wallet </Text>
      <View style={styles.containerTitle}>
        <Icon
          icon={require("@src/assets/images/protect.png")}
          width={120}
          height={142}
        />
      </View>
      <Input
        value={name}
        label="Name wallet"
        placeholder="name wallet"
        onChangeText={onChangeName}
      />
      <View style={styles.containerButtons}>
        <Button
          text="Protect wallet"
          onPress={() => navigation.navigate("MnemonicPhraseScreen", { name })}
          containerStyle={styles.containerButtonApply}
          textStyle={styles.textButtonApply}
        />
        <Button
          text="Do it later"
          onPress={createNewWallet}
          containerStyle={styles.containerButtonDecline}
          textStyle={styles.textButtonDecline}
        />
      </View>
    </ImageBackground>
  );
};