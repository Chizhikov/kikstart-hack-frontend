import * as React from 'react';

import Layout from 'components/Layout';
import ScrollLayout from '@src/components/ScrollLayout';

import Main from './Main';
import NavigationBar from '@src/components/NavigationBar';
import { SafeAreaView } from 'react-native';

export default WelcomeScreen = () => {
  return (
    <Layout backgroundColor='#fff' >
      <SafeAreaView style={{flex: 1}}>
        <Main />
      </SafeAreaView>
    </Layout>
  );
}
