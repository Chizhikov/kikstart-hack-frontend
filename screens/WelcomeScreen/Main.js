import React, { useState, useCallback, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, View, Text, ImageBackground } from 'react-native';

import useColors from '@src/hooks/useColors';
import Button from '@src/components/Button';
import { getStorage, TOKEN_KEY } from '@src/store/system';
import Fonts from '@src/constants/Fonts';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 16,
      flexDirection: 'column',
      paddingBottom: 20,
      justifyContent: 'space-between'
    },
    containerButtonApply: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      backgroundColor: elementColors.backgroundButtonApply,
    },
    containerButtonDecline: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      borderColor: elementColors.borderButton,
    },
    textButtonApply: {
      fontSize: 18,
      marginTop: 5,
      color: elementColors.textButtonApply
    },
    textButtonDecline: {
      fontSize: 16,
      marginTop: 5,
      color: elementColors.textButtonDecline
    },
    containerTitle: {
      flex: 1,
      justifyContent: 'center',
    },
    containerButtons: {

    },
    title: {
      textAlign: 'center',
      fontFamily: Fonts.bold,
      color: elementColors.text,
      fontSize: 30
    },
  })
);

export default Main = () => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});
  const [isLoadingNew, setLoadingNew] = useState(false)
  const [isLoadingRecovery, setLoadingRecovery] = useState(false)

  useEffect(() => {
    (async () => {
      setLoadingNew(true)
      if (await getStorage(TOKEN_KEY)) {
        navigation.navigate('AppStack');
        setLoadingNew(false)
      }
    })();
  }, []);

  const createWallet = () => {
    setLoadingNew(true);
    navigation.navigate("CreateWallet");
    setLoadingNew(false);
  }

  const recoveryWallet = () => {
    setLoadingRecovery(true);
    navigation.navigate("RecoveryWallet");
    setLoadingRecovery(false);
  }

  // useEffect(() => {
  //   if (app.isLoggedIn)
  //     navigation.navigate('AppStack');
  // }, [app.isLoggedIn]);

  return (
    <ImageBackground
      resizeMode="contain"
      source={require("@src/assets/images/dashboard.png")}
      style={styles.container}
    >
      <View style={styles.containerTitle}>
        <Text style={[styles.title, {color: '#2088EC'}]}>G<Text style={styles.title}>WALLET</Text></Text>
      </View>
      <View style={styles.containerButtons}>
        <Button
          text="New wallet"
          onPress={() => createWallet()}
          containerStyle={styles.containerButtonApply}
          textStyle={styles.textButtonApply}
          isLoading={isLoadingNew}
        />
        <Button
          text="Recovery wallet"
          onPress={() => recoveryWallet()}
          containerStyle={styles.containerButtonDecline}
          textStyle={styles.textButtonDecline}
          isLoading={isLoadingRecovery}
        />
      </View>
    </ImageBackground>
  );
};