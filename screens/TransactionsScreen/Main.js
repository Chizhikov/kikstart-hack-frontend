import React, { useState, useCallback, useEffect } from 'react';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { StyleSheet, View, Text } from 'react-native';

import useColors from '@src/hooks/useColors';
import Fonts from '@src/constants/Fonts';
import TouchableElement from '@src/components/TouchableElement';
import Icon from '@src/components/Icon';
import Transaction from '@src/screens/WalletScreen/Transaction';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 16,
      paddingVertical: 20,
      flexDirection: 'column',
    },
    title: {
      fontSize: 30,
      textAlign: 'center',
      color: elementColors.text,
      fontFamily: Fonts.bold,
    },
  })
);

const transactions = [
  { status: 'pending', balanceBGL: 0.34, balanceUSD: 124, statusDate: new Date() },
  { status: 'received', balanceBGL: +0.34, balanceUSD: +124, statusDate: new Date() },
  { status: 'sent', balanceBGL: -0.34, balanceUSD: -124, statusDate: new Date() },
  { status: 'pending', balanceBGL: 0.34, balanceUSD: 124, statusDate: new Date() },
]

export default Main = () => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});

  return (
    <View style={styles.container}>
        <Text style={styles.title}>History</Text>
        {transactions &&
          transactions.map((transaction, index) => {
            return (
              <Transaction
                key={index}
                balanceBGL={transaction.balanceBGL}
                balanceUSD={transaction.balanceUSD}
                status={transaction.status}
                statusDate={transaction.statusDate}
              />
            );
          })}
    </View>
  );
};