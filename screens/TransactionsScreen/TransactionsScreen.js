import * as React from 'react';

import Layout from '@src/components/Layout';
import ScrollLayout from '@src/components/ScrollLayout';

import Main from './Main';

export default TransactionsScreen = () => {
  return (
    <Layout backgroundColor='#fff'>
      <ScrollLayout>
        <Main />
      </ScrollLayout>
    </Layout>
  );
}