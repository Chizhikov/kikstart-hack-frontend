import React, { useState, useCallback, useEffect, Fragment, useReducer } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';

import { getTransaction } from '@src/api';
import useColors from '@src/hooks/useColors';
import Icon from '@src/components/Icon';
import TouchableElement from '@src/components/TouchableElement';
import Fonts from '@src/constants/Fonts';
import moment from 'moment';
import * as Linking from 'expo-linking';
import { addTransaction } from '@src/store/system';


const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1, 
      flexDirection: 'column',
      paddingHorizontal: 16,
      paddingVertical: 20,
    },
    title: {
      marginTop: 20,
      textAlign: 'center',
      fontFamily: Fonts.bold,
      color: elementColors.text,
      fontSize: 30,
      marginBottom: 32,
    },

    containerItem: {
      // flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 16,
      marginBottom: 1,
      borderBottomColor:'rgba(67, 101, 134, 0.25)',
      borderBottomWidth: 1,
    },
    iconItem: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },
    titleItem: {
      flex: 1,
      // textAlign: 'center',
      alignSelf: 'center',
      marginLeft: 24,
      fontFamily: Fonts.regular,
      color: elementColors.text,
      fontSize: 18
    },
    iconView: {
      width: 32,
      height: 32,
      borderRadius: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: elementColors.backgroundScreen,
    },
    containerTransactionItem: {
      flexDirection: 'column',
      marginTop: 28,
      // paddingLeft: 72,
      // paddingRight: 16,
      alignItems: 'flex-start',
    },
    nameItem: {
      fontFamily: Fonts.regular,
      // color: elementColors.textAdditional_Blue,
      fontSize: 16,
      color: 'rgba(67, 101, 134, 0.5)',
    },
    descriptionItem: {
      fontFamily: Fonts.regular,
      color: elementColors.text,
      fontSize: 16
    },
    containerTransaction: {
      flex: 1,
      flexDirection: 'row',
      paddingVertical: 32,
      backgroundColor: '#fff',
      // borderBottomColor:'rgba(67, 101, 134, 0.25)',
      // borderBottomWidth: 1,
      // borderStyle: 'dashed',
    },
    containerService: {
      flex: 1,
      flexDirection: 'row',
    },
    statusContainer: {
      flexDirection: 'column',
      alignItems: 'flex-start',
      marginLeft: 25,
    },
    status: {
      fontSize: 16,
      color: elementColors.text,
      fontFamily: Fonts.regular,
    },
    statusDate: {
      fontSize: 12,
      marginTop: 5,
      color: elementColors.textAdditional_Blue,
      fontFamily: Fonts.light,
    },
    balanceContainer: {
      flexDirection: 'column',
      alignItems: 'flex-end',
    },
    balanceBGL: {
      fontSize: 16,
      color: elementColors.text,
      fontFamily: Fonts.regular,
    },
    paper: {
      paddingHorizontal: 16,
      paddingVertical: 16,
      backgroundColor: '#fff',
    },
    viewBlockchainController: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 55,
      marginBottom: 60
    },
    textBlockchain: {
      marginLeft: 8,
      fontSize: 16,
      color: elementColors.text,
      fontFamily: Fonts.regular,
    }
  })
);

const TransactionInfoItem = ({name, description}) => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});

  return (
    <View style={styles.containerTransactionItem}>
      <Text style={styles.nameItem}>{name}</Text>
      <Text style={styles.descriptionItem}>{description}</Text>
    </View>
  )
}

export default Main = () => {
  const route = useRoute();

  const txHash = route?.params?.txHash;
  const from = route?.params?.from;

  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});
  const blockchain = 'https://bgl.bitaps.com/'
  
  
  const balanceBGL = 0.000043
  const [date, setDate] = useState('');
  const [status, setStatus] = useState('pending');
  const [confirmations, setConfirmations] = useState(0);
  const [inputs, setInputs] = useState([]);
  const [outputs, setOutputs] = useState([]);
  const icon = require("@src/assets/images/sent-status.png");

  useEffect(() => {
    const update = async () => {
      const transaction = await getTransaction(txHash);

      if (!transaction.blockhash) {
        setStatus('pending');
      }
      else {
        setStatus('confirmed');
      }
      setDate(moment(transaction.time * 1000).format('MMMM Do, h:mm a'));
      setConfirmations(transaction.confirmations);
      setInputs(transaction.infoDetails.result.vin);
      setOutputs(transaction.infoDetails.result.vout);

      let sum = 0;
      transaction.infoDetails.result.vout.map(async (out) => {
        await addTransaction(out.scriptPubKey?.addresses[0], {...transaction, status: 'received', amount: out.value });
        if (out.scriptPubKey?.addresses[0] !== from) {
          sum += out.value;
          await addTransaction(from, {...transaction, status: 'sent', amount: sum});
        }
      })
    }

    const interval = setInterval(() => {
      update();
    }, 5000);
    update();

    return () => {
      clearInterval(interval);
    }
  }, []);



  const openLink = () => {
    Linking.openURL(blockchain + txHash); // TODO, update address
  }

  return (
    <View style={styles.container}>

      <View style={styles.paper}>
        <Text style={styles.title}> Transaction Info </Text>

        <TransactionInfoItem name={"Hash"} description={txHash}/>
        <TransactionInfoItem name={"Status"} description={status}/>
        <TransactionInfoItem name={"Date"} description={date}/>
        <TransactionInfoItem name={"Confirmations"} description={confirmations}/>

        {outputs.map((output, i) => (
          <View key={i} style={styles.containerTransaction}>
            <View style={styles.containerService}>
              <Icon icon={output?.scriptPubKey?.addresses[0] == from ? require("@src/assets/images/received-status.png") : require("@src/assets/images/sent-status.png")} height={32} width={32} />
              <View style={styles.statusContainer}>
                <Text style={styles.status}>{status}</Text>
                <Text style={styles.statusDate}>{output?.scriptPubKey?.addresses[0]}</Text>
              </View>
            </View>
            <View style={styles.balanceContainer}>
              <Text
                style={[
                  styles.balanceBGL,
                  {
                    color:
                      status === "received"
                        ? "#45A386"
                        : status === "pending"
                        ? "rgba(67, 101, 134, 0.5)"
                        : "#436586",
                  },
                ]}
              >
                {output.value} BGL
              </Text>
            </View>
          </View>
        ))}

        <TouchableElement style={styles.viewBlockchainController} onPress={openLink}>
          <Icon
            icon={require('@src/assets/images/link.png')}
            width={32}
            height={32}
          />
          <Text style={styles.textBlockchain}>View on Blockchain</Text>
        </TouchableElement>
      </View>
    </View>
  );
};