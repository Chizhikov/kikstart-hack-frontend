import * as React from 'react';

import Layout from '@src/components/Layout';
import ScrollLayout from '@src/components/ScrollLayout';

import Main from './Main';
import NavigationBar from '@src/components/NavigationBar';

export default TransactionInfo = () => {
  return (
    <Layout>
      <ScrollLayout>
        <NavigationBar colorNone/>
        <Main />
      </ScrollLayout>
    </Layout>
  );
}