import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';
import lodash from 'lodash';

import useColors from '@src/hooks/useColors';
import Button from '@src/components/Button';
import Icon from '@src/components/Icon';
import TouchableElement from '@src/components/TouchableElement';
import Input from '@src/components/Input';
import Fonts from '@src/constants/Fonts';

import { createMnemomic, getWalletByMnemonic, importAddress } from '@src/api';
import { setStorage, getStorage, WALLETS } from '@src/store/system';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      paddingTop: 20,
      paddingBottom: 20,
      justifyContent: 'space-between'
    },
    title: {
      textAlign: 'center',
      fontFamily: Fonts.bold,
      color: elementColors.text,
      fontSize: 30
    },
    description: {
      marginTop: 24,
      paddingHorizontal: 16,
      textAlign: 'center',
      fontSize: 14,
      lineHeight: 16.1,
      fontFamily: Fonts.regular,
      color: elementColors.textAdditional_Blue,
    },
    attention: {
      marginTop: 4,
      textAlign: 'center',
      fontSize: 14,
      fontFamily: Fonts.bold,
      color: elementColors.text,
    },

    phraseContainer: {
      marginVertical: 28,
      paddingHorizontal: 40,
      flex: 0,
      flexDirection: 'row',
      flexWrap: 'wrap',
      backgroundColor: '#fff',
    },
    phraseView: {
      display: 'flex',
      flexDirection: 'row',
      borderRadius: 4,
      marginHorizontal: 8,
      marginVertical: 8,
      backgroundColor: elementColors.backgroundScreen,
    },
    phrase: {
      fontSize: 16,
      color: elementColors.text,
      flexWrap: 'wrap',
      paddingHorizontal: 8,
      paddingVertical: 8,
      fontFamily: Fonts.regular,
    },

    containerButtonApply: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      backgroundColor: elementColors.backgroundButtonApply,
    },
    containerButtonDecline: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      borderWidth: 0,
    },
    textButtonApply: {
      fontSize: 18,
      marginTop: 5,
      color: elementColors.textButtonApply
    },
    textButtonDecline: {
      fontSize: 16,
      marginTop: 5,
      color: elementColors.textButtonDecline
    },
    containerButtons: {
      paddingHorizontal: 16,
    },
  })
);

export default Main = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});
  const [mnemonic, setMnemonic] = useState([]);

  useEffect(() => {
    (async () => {
      let mnemonic = await createMnemomic();

      if (route?.params?.wallet) {
        mnemonic = route?.params?.wallet.mnemonic;
      }
      
      setMnemonic(mnemonic.split(' '));
    })();
  }, [route?.params?.wallet])

  const onNextPress = async () => {
    const mnemo = mnemonic.join(' ');
    const wallet = await getWalletByMnemonic(mnemo);

    if (wallet.privateKey) {
      const wallets = await getStorage(WALLETS) || [];

      if (!lodash.find(wallets, { privateKey: wallet.privateKey })) {
        await setStorage(WALLETS, [
          ...wallets,
          { address: wallet.address, privateKey: wallet.privateKey, publicKey: wallet.publicKey, mnemonic: mnemo, name: route.params.name }
        ]);
      }

      await importAddress(wallet.address);

      navigation.navigate('AppStack')
    }
  };


  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}> Mnemonic Phrase </Text>
        <Text style={styles.description}>
          You {mnemonic.length}-word recovery phrase allows you to recover your
          wallet in case you ever lose access to it. Write down the words in the
          correct order from 1 to {mnemonic.length} and store them somewhere
          safe.
        </Text>
        <Text style={styles.attention}>Do not show it to anyone!</Text>
        <View style={styles.phraseContainer}>
          {mnemonic.map((word, index) => {
            return (
              <View style={styles.phraseView} key={index}>
                <Text style={styles.phrase}>{index+1} {word}</Text>
              </View>
            );
          })}
        </View>
      </View>
      <View style={styles.containerButtons}>
        <Button
          text="I save it"
          onPress={onNextPress}
          containerStyle={styles.containerButtonApply}
          textStyle={styles.textButtonApply}
        />
        <Button
          text="Cancel"
          onPress={() => route?.params?.wallet ? navigation.navigate('AppStack') : navigation.goBack()}
          containerStyle={styles.containerButtonDecline}
          textStyle={styles.textButtonDecline}
        />
      </View>
    </View>
  );
};