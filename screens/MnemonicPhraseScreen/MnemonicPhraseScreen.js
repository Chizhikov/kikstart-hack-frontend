import * as React from 'react';

import Layout from '@src/components/Layout';
import ScrollLayout from '@src/components/ScrollLayout';

import Main from './Main';
import NavigationBar from '@src/components/NavigationBar';
import { SafeAreaView } from 'react-native';

export default MnemonicPhraseScreen = () => {
  return (
    <Layout>
      {/* <ScrollLayout> */}
        {/* <NavigationBar /> */}
        <SafeAreaView style={{flex: 1}}>
        <Main />
        </SafeAreaView>
      {/* </ScrollLayout> */}
    </Layout>
  );
}