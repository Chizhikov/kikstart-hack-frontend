import * as React from 'react';

import Layout from '@src/components/Layout';
import ScrollLayout from '@src/components/ScrollLayout';

import Main from './Main';
import NavigationBar from '@src/components/NavigationBar';

export default MyWallets = () => {
  return (
    <Layout backgroundColor='#fff'>
      <ScrollLayout scrolled>
        <NavigationBar />
        <Main />
      </ScrollLayout>
    </Layout>
  );
}