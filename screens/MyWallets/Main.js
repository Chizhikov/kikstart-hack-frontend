import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';

import useColors from '@src/hooks/useColors';
import Icon from '@src/components/Icon';
import TouchableElement from '@src/components/TouchableElement';
import Fonts from '@src/constants/Fonts';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1, 
      flexDirection: 'column',
      paddingHorizontal: 16,
    },
    title: {
      textAlign: 'center',
      fontFamily: Fonts.bold,
      color: elementColors.text,
      fontSize: 30,
      marginBottom: 32,
    },

    containerItem: {
      // flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 16,
      marginBottom: 1,
      borderBottomColor:'rgba(67, 101, 134, 0.25)',
      borderBottomWidth: 1,
    },
    iconItem: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },
    titleItem: {
      flex: 1,
      // textAlign: 'center',
      alignSelf: 'center',
      marginLeft: 24,
      fontFamily: Fonts.regular,
      color: elementColors.text,
      fontSize: 18
    },
    iconView: {
      width: 32,
      height: 32,
      borderRadius: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: elementColors.backgroundScreen,
    }
  })
);

const WalletItem = ({name, active}) => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});

  const editWallet = () => {

  }

  const activeWallet = () => {

  }

  return (
    <View style={styles.containerItem}>
      <TouchableElement onPress={activeWallet} style={styles.iconItem}>
        <View style={styles.iconView}>
        <Icon 
          icon={require('@src/assets/images/wallet.png')}
          width={18}
          height={18}
        />
        </View>
        <Text style={styles.titleItem}>{name}</Text>
      </TouchableElement>
      <TouchableElement style={styles.iconView} onPress={editWallet}>
        <Icon 
          icon={require('@src/assets/images/settings.png')}
          width={18}
          height={89}
        />
      </TouchableElement>
    </View>
  )
}

export default Main = () => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});

  const wallets = [
    {name: "Wallet 1"},
    {name: "Wallet 2"},
    {name: "Wallet 3"},
    {name: "Wallet 4"},
  ];

  return (
    <View style={styles.container}>
      <Text style={styles.title}> My Wallets </Text>
      {wallets.map((wallet, index) => {
        return <WalletItem key={index} name={wallet.name}/>
      })}
    </View>
  );
};