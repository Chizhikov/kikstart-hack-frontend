import * as React from 'react';

import Layout from '@src/components/Layout';
import ScrollLayout from '@src/components/ScrollLayout';

import Main from './Main';

export default WalletScreen = () => {
  return (
    <Layout>
      {/* <ScrollLayout scrolled={false}> */}
        <Main />
      {/* </ScrollLayout> */}
    </Layout>
  );
}
