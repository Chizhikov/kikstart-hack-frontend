import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { StyleSheet, View, Text, BackHandler } from 'react-native';
import lodash from 'lodash';
import moment from 'moment';

import useColors from '@src/hooks/useColors';
import WalletCorousel from './WalletCorousel';
import Fonts from '@src/constants/Fonts';
import TouchableElement from '@src/components/TouchableElement';
import ScrollLayout from '@src/components/ScrollLayout';
import Icon from '@src/components/Icon';
import { getStorage, WALLETS, getTransactions } from '@src/store/system';
import Transaction from './Transaction';
import { BGL_USDT } from '@src/constants/System';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
    },
    containerService: {
      marginTop: 16, 
      paddingTop: 16,
      paddingHorizontal: 16,
      paddingBottom: 21,
      backgroundColor: '#fff',
    },
    titleAdditional: {
      fontSize: 20,
      color: elementColors.textAdditional_Blue,
      fontFamily: Fonts.regular,
    },
    containerButtonService: {
      marginTop: 13,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    buttonTouchable: {
      height: 96,
      flex: 1,
      backgroundColor: 'rgba(233, 243, 253, 0.75)',
      borderRadius: 4,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'flex-start',
      marginRight: 8,
      paddingLeft: 9,
    },
    buttonService: {

    },
    textService: {
      fontSize: 16,
      marginTop: 5,
      color: elementColors.text,
      fontFamily: Fonts.light,
    },
    historyContainer: {
      flex: 1,
      // height: '100%',
      flexDirection: 'column',
      backgroundColor: '#fff',
      paddingHorizontal: 16,
      paddingVertical: 16,
      marginVertical: 19,
    },
    headerHistoryContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: '#fff',
      borderBottomColor:'rgba(67, 101, 134, 0.25)',
      borderBottomWidth: 1,
      // paddingHorizontal: 16,
    },
    seeAllTransactions: {
      fontSize: 14,
      color: elementColors.textAdditional_Blue,
      fontFamily: Fonts.regular,
    },
  })
);

// const transactions = [
//   { status: 'pending', balanceBGL: 0.34, balanceUSD: 124, statusDate: new Date() },
//   { status: 'received', balanceBGL: +0.34, balanceUSD: +124, statusDate: new Date() },
//   { status: 'sent', balanceBGL: -0.34, balanceUSD: -124, statusDate: new Date() },
//   { status: 'pending', balanceBGL: 0.34, balanceUSD: 124, statusDate: new Date() },
// ]

export default Main = () => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});
  const [wallet, setWallet] = useState(null);
  const [wallets, setWallets] = useState([]);
  const [transactions, setTransactions] = useState([]);
  const name = "My Wallet";

  useEffect(() => {
    (async () => {
      const wallets = await getStorage(WALLETS) || [];
      setWallets(wallets);
      setWallet(wallets[0]);
    })();
  }, []);
console.log(transactions)
  useEffect(() => {
    if (wallet) {
      (async () => {
        const transactions = await getTransactions(wallet.address) || [];
        console.log(transactions)
        setTransactions(transactions.map(transaction => ({
          txId: transaction.txid,
          status: !transaction.blockhash ? 'pending' : 'confirmed',
          isReceived: transaction.status === 'sent',
          statusDate: moment(transaction.time * 1000).format('MMMM Do, h:mm a'),
          balanceBGL: transaction.amount,
          balanceUSD: lodash.floor(transaction.amount, 2)
        })));
      })();
    }
  }, [wallet]);

  const hardwareBackPressCustom = useCallback(() => {
    return true;
  }, []);

  useFocusEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', hardwareBackPressCustom)
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', hardwareBackPressCustom)
    };
  }, []);

  return (
    <View  style={styles.container}>
      <WalletCorousel wallets={wallets} onChangeWallet={setWallet} />
      <ScrollLayout>
      <View style={styles.containerService}>
        <Text style={styles.titleAdditional}>Services</Text>
        <View style={styles.containerButtonService}>
          <TouchableElement onPress={() => navigation.navigate('SendToken', { 
            defaultWallet: wallet || wallets[0], 
            defaultActiveIndex: wallet ? lodash.findIndex(wallets, ['address', wallet.address]) : 0 
          })} style={styles.buttonTouchable}>
              <Icon 
                icon={require('@src/assets/images/send.png')}
                height={32}
                width={32}
              />
              <Text style={styles.textService}>Send</Text>
          </TouchableElement>
          <TouchableElement onPress={() => navigation.navigate('ReceiveToken', { wallet })} style={styles.buttonTouchable}>
              <Icon 
                icon={require('@src/assets/images/receive.png')}
                height={32}
                width={32}
              />
              <Text style={styles.textService}>Receive</Text>
          </TouchableElement>
          <TouchableElement onPress={() => navigation.navigate('MnemonicPhraseScreen', { wallet })} style={[styles.buttonTouchable, {marginRight: 0}]}>
              <Icon 
                icon={require('@src/assets/images/cloud.png')}
                height={32}
                width={32}
              />
              <Text style={styles.textService}>Backup</Text>
          </TouchableElement>
        </View>
      </View>
      {/* <ScrollLayout> */}
        <View style={styles.historyContainer}>
          <View style={styles.headerHistoryContainer}>
            <Text style={styles.titleAdditional}>History</Text>
          </View>

          {transactions && transactions.map((transaction, index) => {
            return <Transaction 
                      key={index}
                      wallet={wallet}
                      isReceived={transaction.isReceived}
                      txId={transaction.txId}
                      balanceBGL={transaction.balanceBGL}
                      balanceUSD={transaction.balanceUSD}
                      status={transaction.status}
                      statusDate={transaction.statusDate}
            />
          })}
        </View>
      </ScrollLayout>
    </View>
  )
};