import Carousel, { Pagination } from 'react-native-snap-carousel';
import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useNavigation } from '@react-navigation/native';
import { ImageBackground, StyleSheet, Text, View, Clipboard } from 'react-native';
import useColors from '@src/hooks/useColors';
import Layout from '@src/constants/Layout'
import Constants from 'expo-constants';
import Fonts from '@src/constants/Fonts';
import Icon from '@src/components/Icon';
import lodash from 'lodash'
import TouchableElement from '../../components/TouchableElement';

import { viewAddress } from '@src/utils';
import { BGL_USDT } from '@src/constants/System';
import { getFullBalance, getSpendableBalance, getLockedBalance } from '@src/api';


const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    headerContainer: {
      backgroundColor: '#fff',
      paddingTop: 16,
    },
    title: {
      alignSelf: 'center',
      fontSize: 30,
      fontFamily: Fonts.bold,
      color: elementColors.text,
    },
    cardContainer: {
      height: 170,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    cardContainerData: {
      flexDirection: 'column',
    },
    cardAddContainerData: {
      flexDirection: 'column',
      alignItems: 'center'
    },
    cardContent: {
      justifyContent:'space-evenly',
      flexDirection: 'row',
      alignItems: 'center',
    },
    circleIcon: {
      borderRadius: 27,
      width: 54,
      height: 54,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#fff',
    },
    icon: {
      width: 30.82,
      height: 30.82,
    },
    balanceContainer: {
      flexDirection: 'column',
      alignItems: 'flex-start',
    },
    balanceUSD: {
      fontFamily: Fonts.regular,
      fontSize: 16,
      color: elementColors.textAdditional_White,
    },
    balanceBGL: {
      fontFamily: Fonts.regular,
      fontSize: 21,
      color: elementColors.textAdditional_White,
    },
    balanceBGLPending: {
      fontFamily: Fonts.regular,
      fontSize: 10,
      color: elementColors.textAdditional_White,
    },
    addressContainer: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      marginTop:20,
      marginBottom: -20,
      marginRight: 25,
    },
    address: {
      fontSize: 12,
      fontFamily: Fonts.light,
      color: 'rgba(193, 248, 251, 1)',
      marginRight: 7,
    },
    container: {
      // flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      marginTop: 20,
      backgroundColor: '#fff',
    },
    sliderWallet: {
      // flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
    },
    addWallet: {
      fontSize: 18,
      fontFamily: Fonts.light,
      color: '#fff',
    }
  })
);

const MySlideWallet = (item) => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({ elementColors });

  const backgroundIcon =
    item.index % 3 === 0
      ? require("@src/assets/images/background-card1.png")
      : item.index % 3 === 1
      ? require("@src/assets/images/background-card2.png")
      : require("@src/assets/images/background-card3.png");

  const [balance, setBalance] = useState(0);
  const [pendingBalance, setPendingBalance] = useState(0);
  const { name, address } = item.data || {};

  useEffect(() => {
    const updateBalance = async () => {
      setBalance(lodash.ceil(await getFullBalance(address), 8));
      setPendingBalance(lodash.ceil(await getLockedBalance(address), 8));
    }

    const interval = setInterval(() => {
      updateBalance();
    }, 5000);
    updateBalance();

    return () => {
      clearInterval(interval);
    }
  }, [address]);   

  if (!address) 
    return (
      <ImageBackground
        resizeMode='cover'
        source={backgroundIcon}
        style={styles.cardContainer}
      >
        <TouchableElement style={styles.cardAddContainerData} onPress={() => navigation.navigate('CreateWallet')}>
          <Icon 
            icon={require('@src/assets/images/plus.png')}
            height={24}
            width={24}
          />
          <Text style={styles.addWallet}>Add new wallet</Text>
        </TouchableElement>
      </ImageBackground>
    )

  return (
    <ImageBackground
      resizeMode='cover'
      source={backgroundIcon}
      style={styles.cardContainer}
    >
      <View style={styles.cardContainerData}>
        <View style={styles.cardContent}>
          <View style={styles.circleIcon}>
            <Icon
              icon={require('@src/assets/images/coin-bgl.png')}
              height={30.82}
              width={30.82}
            />
          </View>
          <View style={styles.balanceContainer}>
            <Text style={styles.balanceUSD}>{lodash.floor(balance * BGL_USDT, 2)} $</Text>
            <Text style={styles.balanceBGL}>{balance} BGL</Text>
            <Text style={styles.balanceBGLPending}>{pendingBalance} locked</Text>
          </View>
        </View>
        <TouchableElement style={styles.addressContainer} onPress={() => Clipboard.setString(address)}>
          <Text style={styles.address}>{viewAddress(address)}</Text>
          <Icon
            icon={require('@src/assets/images/copy-white.png')}
            height={18}
            width={18}
          />
        </TouchableElement>
      </View>
    </ImageBackground>
  );
}

const WalletCorousel = ({ wallets, onChangeWallet }) => {
  const elementColors = useColors();
  const styles = Styles({ elementColors });
  const [activeIndex, setActiveIndex] = useState(0);
  const [index, setIndex] = useState(0)
  const [wallet, setWallet] = useState(null);
  const entiresDotsLength = wallets.length + 1;
  const countWallets = wallets.length;

  useEffect(() => {
    onChangeWallet(wallet);
  }, [wallet]);

  const renderItem = ({ item, index }) => {
    return <MySlideWallet data={item} index={index}/>;
  };

  return (
    <View style={styles.headerContainer}>
      <Text style={styles.title}>{index < wallets.length ? wallets[index].name || `Wallet ${index + 1}` : 'Add New Wallet'}</Text>
      <View style={styles.container}>
        <View style={styles.sliderWallet}>
          <Carousel
            data={[...wallets, {}]}
            renderItem={renderItem}
            sliderWidth={Layout.window.width}
            itemWidth={Layout.window.width-70}
            onSnapToItem={(index) => {
              setActiveIndex(index%entiresDotsLength); 
              setIndex(index);
              setWallet(wallets[index]);
            }}
            layout={"default"}
          />
        </View>
        <Pagination
          dotsLength={entiresDotsLength}
          activeDotIndex={activeIndex}
          dotStyle={{
            width: 6,
            height: 6,
            borderRadius: 3,
            marginHorizontal: 1.25,
            backgroundColor: "#2088EC",
          }}
          inactiveDotStyle={{
              // Define styles for inactive dots here
              backgroundColor: '#D8D8D8',
          }}
          inactiveDotOpacity={0.8}
          inactiveDotScale={0.7}
        />
      </View>
    </View>
  );
};

export default WalletCorousel;