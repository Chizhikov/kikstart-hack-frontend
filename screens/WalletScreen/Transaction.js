import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, View, Text } from 'react-native';

import useColors from '@src/hooks/useColors';
import Fonts from '@src/constants/Fonts';
import TouchableElement from '@src/components/TouchableElement';
import Icon from '@src/components/Icon';
import moment from 'moment';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      paddingVertical: 32,
      backgroundColor: '#fff',
      borderBottomColor:'rgba(67, 101, 134, 0.25)',
      borderBottomWidth: 1,
      // borderStyle: 'dashed',
    },
    containerService: {
      flex: 1,
      flexDirection: 'row',
    },
    statusContainer: {
      flexDirection: 'column',
      alignItems: 'flex-start',
      marginLeft: 25,
    },
    status: {
      fontSize: 16,
      color: elementColors.text,
      fontFamily: Fonts.regular,
    },
    statusDate: {
      fontSize: 12,
      marginTop: 5,
      color: elementColors.textAdditional_Blue,
      fontFamily: Fonts.light,
    },
    balanceContainer: {
      flexDirection: 'column',
      alignItems: 'flex-end',
    },
    balanceBGL: {
      fontSize: 16,
      color: elementColors.text,
      fontFamily: Fonts.regular,
    },
    balanceUSD: {
      fontSize: 12,
      marginTop: 5,
      color: elementColors.textAdditional_Blue,
      fontFamily: Fonts.regular,
    },
  })
);

export default Transaction = ({status, statusDate, balanceUSD, balanceBGL, txId, wallet, isReceived}) => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});
  const date = statusDate;

  const icon =
    status === "pending"
      ? require("@src/assets/images/pending-status.png")
      : isReceived
      ? require("@src/assets/images/sent-status.png")
      : require("@src/assets/images/received-status.png");

  return (
    <TouchableElement style={styles.container} onPress={() => navigation.navigate('TransactionInfo', { txHash: txId, from: wallet.address })}>
      <View style={styles.containerService}>
        <Icon icon={icon} height={32} width={32} />
        <View style={styles.statusContainer}>
          <Text style={styles.status}>{status}</Text>
          <Text style={styles.statusDate}>{date}</Text>
        </View>
      </View>
      <View style={styles.balanceContainer}>
        <Text
          style={[
            styles.balanceBGL,
            {
              color:
                status === "received"
                  ? "#45A386"
                  : status === "pending"
                  ? "rgba(67, 101, 134, 0.5)"
                  : "#436586",
            },
          ]}
        >
          {balanceBGL} BGL
        </Text>
        <Text style={styles.balanceUSD}>{balanceUSD} $</Text>
      </View>
    </TouchableElement>
  );
};