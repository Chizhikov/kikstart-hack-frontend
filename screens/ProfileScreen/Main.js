import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';

import useColors from '@src/hooks/useColors';
// import Button from '@src/components/Button';
import Icon from '@src/components/Icon';
import Fonts from '@src/constants/Fonts';
import TouchableElement from '@src/components/TouchableElement';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1, 
      flexDirection: 'column',
      paddingHorizontal: 8,
    },
    title: {
      textAlign: 'center',
      fontFamily: Fonts.bold,
      color: elementColors.text,
      fontSize: 30,
      marginBottom: 32,
    },
    menuButtons: {
      flex: 1,
      flexDirection: 'row'
    },
    containerButton: {
      marginTop: 20,
      flex: 0,
      flexDirection: 'row',
      padding: 20,
    },
    profileItem: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 16,
      paddingVertical: 16,
      marginBottom: 1,
      borderBottomColor:'rgba(67, 101, 134, 0.25)',
      borderBottomWidth: 1,
    },
    profileView: { 
      flexDirection: 'row',
    },
    iconContainer: {
      width: 32,
      height: 32,
      borderRadius: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: elementColors.backgroundScreen,
    },
    name: {
      alignSelf: 'center',
      marginLeft: 24,
      fontFamily: Fonts.regular,
      color: elementColors.text,
      fontSize: 18
    }
  })
);

const ProfileItem = ({ icon, name, onPress }) => {
  const elementColors = useColors();
  const styles = Styles({elementColors});

  return (
    <TouchableElement style={styles.profileItem} onPress={onPress}>
      <View style={styles.profileView}>
        <View style={styles.iconContainer}>
          <Icon 
            icon={icon}
            width={18}
            height={18}
          />
        </View>
        <Text style={styles.name}>{name}</Text>
      </View>
    </TouchableElement>
  )
}

export default Main = () => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});

  return (
    <View style={styles.container}>
      <Text style={styles.title}> Profile </Text>
      <ProfileItem 
        icon={require('@src/assets/images/wallet.png')}
        name='Wallets'
        onPress={() => navigation.navigate('MyWallets')}
      />
      <ProfileItem 
        icon={require('@src/assets/images/settings.png')}
        name='Settings'
        onPress={() => {}}
      />
      {/* <ProfileItem 
        icon={require('@src/assets/images/download.png')}
        name='Recovery'
        onPress={() => navigation.navigate('RecoveryWallet')}
      />
      <ProfileItem 
        icon={require('@src/assets/images/lock.png')}
        name='Security'
        onPress={() => navigation.navigate('MnemonicPhraseScreen')}
      /> */}
    </View>
  );
};