import Carousel, { Pagination } from 'react-native-snap-carousel';
import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useNavigation } from '@react-navigation/native';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import useColors from '@src/hooks/useColors';
import Layout from '@src/constants/Layout'
import Constants from 'expo-constants';
import Fonts from '@src/constants/Fonts';
import { viewAddress } from '@src/utils';
import { BGL_USDT } from '@src/constants/System';
import Icon from '@src/components/Icon';
import lodash from 'lodash'
import TouchableElement from '@src/components/TouchableElement';
import { getFullBalance,getLockedBalance } from '@src/api';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    headerContainer: {
      backgroundColor: '#fff',
      paddingTop: 16,
    },
    title: {
      marginLeft: 16,
      textAlign: 'left',
      fontSize: 16,
      fontFamily: Fonts.regular,
      color: elementColors.text,
    },
    cardContainer: {
      height: 170,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    cardContainerData: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      marginLeft: 24
    },
    circleIcon: {
      borderRadius: 27,
      width: 54,
      height: 54,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#fff',
    },
    icon: {
      width: 30.82,
      height: 30.82,
    },
    balanceContainer: {
      marginLeft: 17,
      flexDirection: 'column',
      alignItems: 'flex-start',
    },
    balanceUSD: {
      marginTop: 4,
      fontFamily: Fonts.regular,
      fontSize: 14,
      color: elementColors.textAdditional_White,
    },
    balanceBGL: {
      marginTop: 4,
      fontFamily: Fonts.regular,
      fontSize: 21,
      fontSize: 16,
      color: elementColors.textAdditional_White,
    },
    balanceBGLPending: {
      fontFamily: Fonts.regular,
      fontSize: 10,
      color: elementColors.textAdditional_White,
    },
    addressContainer: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      marginTop:20,
      marginBottom: -20,
      marginRight: 25,
    },
    address: {
      fontSize: 12,
      fontFamily: Fonts.light,
      color: 'rgba(193, 248, 251, 1)',
      marginRight: 7,
    },
    container: {
      // flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      marginTop: 20,
      backgroundColor: elementColors.textAdditional_White,
    },
    sliderWallet: {
      // flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
    },
    name: {
      fontSize: 18,
      fontFamily: Fonts.bold,
      color: '#fff',
      // marginRight: 7,
    }
  })
);

const carouselItems = [
{
    address: "0xhDs7df78dJer98v9",
    balanceUSD: 4350,
    balanceBGL: 0.04324,
    name: "My Wallet 1"
},
{
  address: "0x09scj92n29w0ec",
  balanceUSD: 1350,
  balanceBGL: 0.00524,
  name: "My Wallet 2"
},
{
  address: "0x7s70ce0ce0r70we0c",
  balanceUSD: 9350,
  balanceBGL: 0.24324,
  name: "My Wallet 3"
},
]

const MySlideWallet = (item) => {
  const elementColors = useColors();
  const styles = Styles({ elementColors });

  const backgroundIcon =
    item.index % 3 === 0
      ? require("@src/assets/images/background-card1.png")
      : item.index % 3 === 1
      ? require("@src/assets/images/background-card2.png")
      : require("@src/assets/images/background-card3.png");

  const { name, address } = item.data

  const [balance, setBalance] = useState(0);
  const [pendingBalance, setPendingBalance] = useState(0);

  useEffect(() => {
    const updateBalance = async () => {
      setBalance(lodash.ceil(await getFullBalance(address), 8));
      setPendingBalance(lodash.ceil(await getLockedBalance(address), 8));
    }

    const interval = setInterval(() => {
      updateBalance();
    }, 5000);
    updateBalance();

    return () => {
      clearInterval(interval);
    }
  }, []);

  return (
    <ImageBackground
      resizeMode="cover"
      source={backgroundIcon}
      style={styles.cardContainer}
    >
      <View style={styles.cardContainerData}>
        <View style={styles.circleIcon}>
          <Icon
            icon={require("@src/assets/images/coin-bgl.png")}
            height={30.82}
            width={30.82}
          />
        </View>
        <View style={styles.balanceContainer}>
          <Text style={styles.name}>{name || viewAddress(address)}</Text>
          <Text style={styles.balanceUSD}>{lodash.floor(balance * BGL_USDT, 2)} $</Text>
          <Text style={styles.balanceBGL}>{balance} BGL</Text>
          <Text style={styles.balanceBGLPending}>{pendingBalance} locked</Text>
        </View>
      </View>
    </ImageBackground>
  );
}

const WalletCorousel = ({ wallets, onChangeWallet, defaultActiveIndex }) => {
  const [wallet, setWallet] = useState(wallets[defaultActiveIndex]);
  const elementColors = useColors();
  const styles = Styles({ elementColors });
  const [activeIndex, setActiveIndex] = useState(defaultActiveIndex);
  const entiresDotsLength = wallets.length;

  const renderItem = ({ item, index }) => {
    return <MySlideWallet data={item} index={index}/>;
  };

  useEffect(() => {
    onChangeWallet(wallet);
  }, [wallet]);

  return (
    <View style={styles.headerContainer}>
      <Text style={styles.title}>From</Text>
      <View style={styles.container}>
        <View style={styles.sliderWallet}>
          <Carousel
            apparitionDelay={200}
            firstItem={activeIndex}
            data={wallets}
            renderItem={renderItem}
            sliderWidth={Layout.window.width}
            itemWidth={Layout.window.width-70}
            onSnapToItem={(index) => {
              setActiveIndex(index%entiresDotsLength); 
              setWallet(wallets[index]);
            }}
            layout={"default"}
          />
        </View>
        <Pagination
          dotsLength={entiresDotsLength}
          activeDotIndex={activeIndex}
          dotStyle={{
            width: 6,
            height: 6,
            borderRadius: 3,
            marginHorizontal: 1.25,
            backgroundColor: "#2088EC",
          }}
          inactiveDotStyle={{
              // Define styles for inactive dots here
              backgroundColor: '#D8D8D8',
          }}
          inactiveDotOpacity={0.8}
          inactiveDotScale={0.7}
        />
      </View>
    </View>
  );
};

export default WalletCorousel;