import React, { useState, useCallback, useEffect } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { StyleSheet, View, Text, Platform } from 'react-native';
import lodash from 'lodash';

import useColors from '@src/hooks/useColors';
import Button from '@src/components/Button';
import Input from '@src/components/Input';
import { sendBGL } from '@src/api';
import { getStorage, WALLETS } from '@src/store/system';
import WalletCorousel from './WalletCorousel';
import Fonts from '@src/constants/Fonts';
import { BGL_USDT } from '@src/constants/System';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      paddingTop: 20,
      paddingBottom: 40,
    },
    containerButton: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
    },
    title: {
      fontSize: 30,
      textAlign: 'center',
      color: elementColors.text,
      fontFamily: Fonts.bold,
    },
    walletTitleLabel: {
      paddingTop: 20,
      paddingLeft: 20,
      fontSize: 14,
      color: elementColors.text
    },
    walletTitle: {
      paddingLeft: 20,
      fontSize: 16,
      color: elementColors.text
    },
    containerBalance: {
      alignSelf: 'center',
      paddingBottom: 20,
    },
    inputContainer: {
      flex: 1
    },
    fee: {
      alignSelf: 'center',
      paddingBottom: 12,
    },
    containerButtonApply: {
      flex: 1,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      backgroundColor: elementColors.backgroundButtonApply,
    },
    textButtonApply: {
      fontSize: 18,
      marginTop: 5,
      color: elementColors.textButtonApply
    },
    main: {
      paddingHorizontal: 16,
    },
    feeUSD: {
      textAlign: 'center',
      fontFamily: Fonts.regular,
      fontSize: 16,
      marginTop: 30,
      color: elementColors.text
    },
    feeBGL: {
      textAlign: 'center',
      fontFamily: Fonts.regular,
      fontSize: 14,
      marginTop: 5,
      marginBottom: 34,
      color: elementColors.textAdditional_Blue,
    }
  })
);

export default Main = () => {
  const route = useRoute();
  const navigation = useNavigation();

  const defWallet = route && route.params && route.params.defaultWallet;
  const defActiveIndex = route && route.params && route.params.defaultActiveIndex;

  const [wallet, setWallet] = useState(defWallet);
  const [wallets, setWallets] = useState([]);

  const elementColors = useColors();
  const styles = Styles({elementColors});
  const [amount, setAmount] = useState('');
  const [to, setTo] = useState('');
  const [error, setError] = useState('');
  const [reload, setReload] = useState(false);

  const [feeUSD, setFeeUSD] = useState(lodash.floor(0.000002 * BGL_USDT, 2));
  const [feeBGL, setFeeBGL] = useState(0.000002);

  useEffect(() => {
    (async () => {
      setWallets(await getStorage(WALLETS) || []);
    })();
  }, [reload]);

  useEffect(() => {
    setWallet(defWallet);
  }, [defWallet])

  const onAmountChange = (text) => {
    setAmount(text);
  }

  const onSendPress = async () => {
    //  const { error, result } = await sendBGL("KyNV2RGWajXqVDhSyh4WswDeAjttxdCiQFwopRLkUXRrkPsGkL2S", "bgl1qcsgeusrhkqjar265vv7f3djjdqv30dvqrpwjtc", 2.5);
    const { error, result } = await sendBGL(wallet.privateKey, to, Number(amount));

    if (error) {
      setError(error);
    }
    else {
      navigation.navigate('TransactionInfo', { txHash: result, from: wallet.address })
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>New Transfer</Text>
      <WalletCorousel wallets={wallets} onChangeWallet={setWallet} defaultActiveIndex={defActiveIndex || 0} />
      <View style={styles.main}>
        <View style={styles.inputContainer}>
          <Input
            value={to}
            label="To"
            placeholder="To address"
            clickableIcon={{
              onPress: () => {},
              icon: require("@src/assets/images/scan.png"),
            }}
            onChangeText={setTo}
            errorText={""}
          />
        </View>

        <View style={styles.inputContainer}>
          <Input
            value={amount}
            label="Amount"
            placeholder="amount"
            onChangeText={onAmountChange}
            staticText="BGL"
            errorText={error}
          />
        </View>
        {/* <Text style={styles.feeUSD}>${feeUSD}</Text> */}
        <Text style={styles.feeBGL}>Fee {feeBGL} BGL</Text>
        <Button
          text="Send"
          onPress={onSendPress}
          containerStyle={styles.containerButtonApply}
          textStyle={styles.textButtonApply}
        />
      </View>
    </View>
  );
};