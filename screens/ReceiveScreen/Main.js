import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Clipboard } from 'react-native';
import PropTypes from 'prop-types';
import QRCode from 'react-native-qrcode-svg';
import * as Linking from 'expo-linking';
import { useRoute } from '@react-navigation/native';


import useColors from '@src/hooks/useColors';
import CopyText from '@src/components/CopyText';
import Icon from '@src/components/Icon';
import TouchableElement from '@src/components/TouchableElement';
import Fonts from '@src/constants/Fonts';
import Button from '@src/components/Button';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      alignItems: 'center',
      paddingHorizontal: 16,
    },
    textTitle: {
      fontSize: 30,
      textAlign: 'center',
      color: elementColors.text,
      fontFamily: Fonts.bold,
    },
    name: {
      marginTop: 24,
      marginBottom: 18,
      fontSize: 18,
      textAlign: 'center',
      color: elementColors.text,
      fontFamily: Fonts.regular,
    },

    titleContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 13,
    },
    iconContainer: {
      marginRight: 8,
    },

    descriptionContainer: {
      marginTop: 20,
    },
    textDescription: {
      // fontFamily: Fonts.sfProText.regular,
      textAlign: 'center',
      fontSize: 15,
      color: elementColors.textMain
    },
    textDescriptionCurrency: {
      // fontFamily: Fonts.sfProText.semibold,
    },
    copyTextContainer: {
      marginTop: 10,
    },

    containerButtonService: {
      marginTop: 75,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    buttonTouchable: {
      height: 96,
      flex: 1,
      backgroundColor: 'rgba(233, 243, 253, 0.75)',
      borderRadius: 4,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'flex-start',
      marginRight: 8,
      paddingLeft: 9,
    },
    buttonService: {

    },
    textService: {
      fontSize: 16,
      marginTop: 5,
      color: elementColors.text,
      fontFamily: Fonts.regular,
    },
  })
);

const Main = () => {
  const route = useRoute();
  const wallet = route.params.wallet;
  console.log(wallet)
  const elementColors = useColors();
  const [styles, updateStyles] = useState(Styles({
    elementColors
  }));

  var blockchain = 'https://bgl.bitaps.com/'

  useEffect(() => {
    updateStyles(Styles({ elementColors }));
  }, [elementColors]);

  const copyText = () => {
    Clipboard.setString(wallet.address);
  }

  const openLink = () => {
    Linking.openURL(blockchain+wallet.address);
  }

  const generatedAddress = () => {

 }
  return (
    <View style={styles.container}>
        <Text style={styles.textTitle}>Receive</Text>
        <Text style={styles.name}>BITGESELL ADDRESS</Text>
      <QRCode value={wallet.address} size={180} />
      <View style={[styles.rowContainer, styles.descriptionContainer]}>
        <Text style={styles.textDescription}>{wallet.address}</Text>
      </View>
      <View style={styles.containerButtonService}>
          <TouchableElement onPress={() => generatedAddress()} style={styles.buttonTouchable}>
              <Icon 
                icon={require('@src/assets/images/generate.png')}
                height={32}
                width={32}
              />
              <Text style={styles.textService}>Generate address</Text>
          </TouchableElement>
          <TouchableElement onPress={() => copyText()} style={styles.buttonTouchable}>
              <Icon 
                icon={require('@src/assets/images/copy-blue.png')}
                height={32}
                width={32}
              />
              <Text style={styles.textService}>Copy</Text>
          </TouchableElement>
          <TouchableElement onPress={() => openLink()} style={[styles.buttonTouchable, {marginRight: 0}]}>
              <Icon 
                icon={require('@src/assets/images/link.png')}
                height={32}
                width={32}
              />
              <Text style={styles.textService}>View on Blockchain</Text>
          </TouchableElement>
        </View>
    </View>
  );
};

export default Main;