import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';
import { useForm } from 'react-hook-form';
import lodash from 'lodash';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import useColors from '@src/hooks/useColors';
import Button from '@src/components/Button';
import Input from '@src/components/Input';

import { getWalletByMnemonic, importAddress } from '@src/api';
import { setStorage, getStorage, WALLETS } from '@src/store/system';
import Fonts from '../../constants/Fonts';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      flex: 1, 
      flexDirection: 'column',
      paddingBottom: 40,
      paddingHorizontal: 8,
    },
    title: {
      marginTop: 20,
      alignSelf: 'center',
      fontSize: 30,
      marginBottom: 20,
      fontFamily: Fonts.bold,
      color: elementColors.text,
    },
    description: {
      marginTop: 14,
      textAlign: 'center',
      fontSize: 14,
      marginBottom: 20,
      fontFamily: Fonts.bold,
      color: elementColors.text,
    },
    menuButtons: {
      flex: 1,
      flexDirection: 'row'
    },
    inputContainer: {
      width: '100%',
    },
    containerButtonApply: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      backgroundColor: elementColors.backgroundButtonApply,
    },
    containerButtonDecline: {
      flex: 0,
      paddingTop: 10,
      paddingBottom: 14,
      marginTop: 8,
      borderWidth: 0,
    },
    textButtonApply: {
      fontSize: 18,
      marginTop: 5,
      color: elementColors.textButtonApply
    },
    textButtonDecline: {
      fontSize: 16,
      marginTop: 5,
      color: elementColors.textButtonDecline
    },
    containerTitle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    containerButtons: {

    },
  })
);

export default Main = () => {
  const navigation = useNavigation();
  const elementColors = useColors();
  const styles = Styles({elementColors});
  const wordsArr = Array(12)
  for(var i = 0; i < 12; i++) {
    wordsArr[i] = `word${i}`
  }
  const { 
    register, 
    setValue, 
    watch, 
    handleSubmit, 
    setError, 
    errors 
  } = useForm(
    {
      mode: "onSubmit",
      resolver: yupResolver(
        yup.object().shape({
          word1: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word2: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word3: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word4: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word5: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word6: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word7: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word8: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word9: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word10: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word11: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
          word12: yup.string("").min(3, "Need 3 symbols for word").required("Mnemonic Word is required."),
        })
      ),
      defaultValues: {
        word1: "road",
        word2: "flat",
        word3: "paper",
        word4: "salt",
        word5: "drip",
        word6: "sport",
        word7: "sight",
        word8: "excuse",
        word9: "move",
        word10: "ritual",
        word11: "pill",
        word12: "produce"
      }
    }
  );
  const { word1, word2, word3, word4, word5, word6, word7, word8, word9, word10, word11, word12 } = watch();
  const words = watch();

  useEffect(() => {
    for(var i = 0; i < 12; i++) {
      register(`word${i+1}`)
    }
  }, [])

  const onSubmitPress = async () => {
    const mnemonic = [word1, word2, word3, word4, word5, word6, word7, word8, word9, word10, word11, word12].join(' ');
    const wallet = await getWalletByMnemonic(mnemonic);

    if (wallet.privateKey) {
      const wallets = await getStorage(WALLETS) || [];

      if (!lodash.find(wallets, { privateKey: wallet.privateKey })) {
        await setStorage(WALLETS, [
          ...wallets,
          { address: wallet.address, privateKey: wallet.privateKey, publicKey: wallet.publicKey, mnemonic }
        ]);
      }

      await importAddress(wallet.address);

      navigation.navigate('AppStack')
    }
    else {
      setError("word12", {
        type: "manual",
        message: "We can't restore your wallet from mnemonic phrase"
      })
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}> Recovery Wallet </Text>
      <Text style={styles.description}>
        Type your 12-word phrase with space. Put spaces between words.
      </Text>
      {/* {wordsArr && wordsArr.map((wordArr, index, array) => {
        return <View style={styles.inputContainer}>
                <Input 
                    value={words[`word${index+1}`]}
                    label={`${index+1} word`}
                    placeholder={`${index+1} word`}
                    onChangeText={(word) => {
                      console.log(errors[`word${index+1}`])
                      if (errors[`word${index+1}`] && errors[`word${index+1}`].message)
                        setValue(`word${index+1}`, word, { shouldValidate: true });
                      else 
                        setValue(`word${index+1}`, word);
                    }}
                    errorText={'errors[`word${index+1}`]' || ''}
                />
                </View>
      })} */}
      <View style={styles.inputContainer}>
      <Input 
          value={word1}
          label="1 word"
          placeholder="1 word"
          onChangeText={(word) => {
            if (errors?.word1?.message)
              setValue('word1', word, { shouldValidate: true });
            else 
              setValue('word1', word);
          }}
          errorText={errors?.word1?.message || ''}
      />
      </View>

      <View style={styles.inputContainer}>
      <Input 
          value={word2}
          label="2 word"
          placeholder="2 word"
          onChangeText={(word) => {
            if (errors?.word2?.message)
              setValue('word2', word, { shouldValidate: true });
            else 
              setValue('word2', word);
          }}
          errorText={errors?.word2?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word3}
          label="3 word"
          placeholder="3 word"
          onChangeText={(word) => {
            if (errors?.word3?.message)
              setValue('word3', word, { shouldValidate: true });
            else 
              setValue('word3', word);
          }}
          errorText={errors?.word3?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word4}
          label="4 word"
          placeholder="4 word"
          onChangeText={(word) => {
            if (errors?.word4?.message)
              setValue('word4', word, { shouldValidate: true });
            else 
              setValue('word4', word);
          }}
          errorText={errors?.word4?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word5}
          label="5 word"
          placeholder="5 word"
          onChangeText={(word) => {
            if (errors?.word5?.message)
              setValue('word5', word, { shouldValidate: true });
            else 
              setValue('word5', word);
          }}
          errorText={errors?.word5?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word6}
          label="6 word"
          placeholder="6 word"
          onChangeText={(word) => {
            if (errors?.word6?.message)
              setValue('word6', word, { shouldValidate: true });
            else 
              setValue('word6', word);
          }}
          errorText={errors?.word6?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word7}
          label="7 word"
          placeholder="7 word"
          onChangeText={(word) => {
            if (errors?.word7?.message)
              setValue('word7', word, { shouldValidate: true });
            else 
              setValue('word7', word);
          }}
          errorText={errors?.word7?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word8}
          label="8 word"
          placeholder="8 word"
          onChangeText={(word) => {
            if (errors?.word8?.message)
              setValue('word8', word, { shouldValidate: true });
            else 
              setValue('word8', word);
          }}
          errorText={errors?.word8?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word9}
          label="9 word"
          placeholder="9 word"
          onChangeText={(word) => {
            if (errors?.word9?.message)
              setValue('word9', word, { shouldValidate: true });
            else 
              setValue('word9', word);
          }}
          errorText={errors?.word9?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word10}
          label="10 word"
          placeholder="10 word"
          onChangeText={(word) => {
            if (errors?.word10?.message)
              setValue('word10', word, { shouldValidate: true });
            else 
              setValue('word10', word);
          }}
          errorText={errors?.word1?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word11}
          label="11 word"
          placeholder="11 word"
          onChangeText={(word) => {
            if (errors?.word11?.message)
              setValue('word11', word, { shouldValidate: true });
            else 
              setValue('word11', word);
          }}
          errorText={errors?.word11?.message || ''}
      />
      </View>
      <View style={styles.inputContainer}>
      <Input 
          value={word12}
          label="12 word"
          placeholder="12 word"
          onChangeText={(word) => {
            if (errors?.word12?.message)
              setValue('word12', word, { shouldValidate: true });
            else 
              setValue('word12', word);
          }}
          errorText={errors?.word12?.message || ''}
      />
      </View>
      <View style={styles.containerButtons}>
        <Button
          text="Ok"
          onPress={onSubmitPress}
          containerStyle={styles.containerButtonApply}
          textStyle={styles.textButtonApply}
        />
        <Button
          text="Cancel"
          onPress={() => navigation.goBack()}
          containerStyle={styles.containerButtonDecline}
          textStyle={styles.textButtonDecline}
        />
      </View>
    </View>
  );
};