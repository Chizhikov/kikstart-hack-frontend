import lodash from 'lodash'

export const viewAddress = (address) => {
  return `${lodash.truncate(address, { length: 15 })}${address.slice(-5, address.length)}`
}