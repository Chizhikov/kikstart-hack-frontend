import lodash from 'lodash';
const jsbgl = require('./jsbgl');


export const createMnemomic = () => {
  console.log(jsbgl.generateEntropy)
  const entropy = jsbgl.generateEntropy();
  const mnemonic = jsbgl.entropyToMnemonic(entropy);

  return mnemonic;
}

export const getWalletByMnemonic = (mnemonic) => {
  const wallet = new jsbgl.Wallet({ from: mnemonic });

  return wallet;
}

export const getBGLAccountFromWallet = (wallet) => {
  return wallet.getAddress(0);
}

export const sendBGL = async (pk, to, amount) => {
  const fee = 0.00001;
  const from = jsbgl.publicKeyToAddress( jsbgl.privateToPublicKey(pk) );

  const listUnspentTxs = await apiGetListUnspentTxs(from);

  if (!listUnspentTxs.length) {
    throw new Error('No balance');
  }

  const availableBalance = lodash.reduce(listUnspentTxs, (sum, tx) => {
    return sum + tx.amount;
  }, 0);

  if (availableBalance < (amount + fee)) {
    throw new Error('No enough balance');
  }

  const transaction = new jsbgl.Transaction({ version: 2, lockTime: 32451 });

  lodash.forEach(listUnspentTxs, (tx) => {
    transaction.addInput({ value: tx.amount * 100000000, txId: tx.txid, vOut: tx.vout });
  });

  transaction.addOutput({ value: amount * 100000000, address: to });
  transaction.addOutput({ value: lodash.floor(availableBalance - amount - fee, 8) * 100000000, address: from });
  // console.log(transaction)

  transaction.signInput(0, {
    privateKey: pk,
    address: from,
    sigHashType: 0x00000001
  });
  const signedTransaction = transaction.serialize();

  return await apiSendTransaction(signedTransaction);
}

export const getLockedBalance = async (address) => {
  return await jsbgl.getLockedBalance(address);
}

export const getSpendableBalance = async (address) => {
  return await jsbgl.getSpendableBalance(address);
}

export const getFullBalance = async (address) => {
  return await jsbgl.getFullBalance(address);
}

export const importAddress = async (address) => {
  return await jsbgl.importAddress(address);
}

export const getTransaction = async (hash) => {
  return await jsbgl.getTransaction(hash);
}

// const apiGetReceivedBalance = async (address, confirmations = 1) => {
//   const data = JSON.stringify({
//     "jsonrpc": "1.0",
//     "method": "getreceivedbyaddress",
//     "params": [address, confirmations]
//   });

//   const { data: { result } } = await rpcNode.post('', data);

//   return result;
// }

// const apiGetListUnspentTxs = async (address) => {
//   const data = JSON.stringify({
//     "jsonrpc": "1.0",
//     "method": "listunspent",
//     "params": [1,9999999,[address]]
//   });

//   const { data: { result } } = await rpcNode.post('', data);

//   return result;
// }

// const apiSendTransaction = async (rawTransaction) => {
//   const imputData = JSON.stringify({
//     "jsonrpc": "1.0",
//     "method": "sendrawtransaction",
//     "params": [rawTransaction]
//   });

//   const { data } = await rpcNode.post('', imputData);

//   return data;
// }

// const apiGetTransaction = async (hash) => {
//   const data = JSON.stringify({
//     "jsonrpc": "1.0",
//     "method": "gettransaction",
//     "params": [hash]
//   });

//   const { data: { result } } = await rpcNode.post('', data);

//   return result;
// }


// const createMnemomic = () => {
//   console.log(jsbgl.generateEntropy)
//   const entropy = jsbgl.generateEntropy();
//   const mnemonic = jsbgl.entropyToMnemonic(entropy);

//   return mnemonic;
// }


// const getWalletByMnemonic = (mnemonic) => {
//   const wallet = new jsbgl.Wallet({ from: mnemonic });

//   return wallet;
// }

// const getBGLAccountFromWallet = (wallet) => {
//   return wallet.getAddress(0);
// }

// const sendBGL = async (pk, to, amount) => {
//   const fee = 0.00001;
//   const from = jsbgl.publicKeyToAddress( jsbgl.privateToPublicKey(pk) );

//   const listUnspentTxs = await apiGetListUnspentTxs(from);

//   if (!listUnspentTxs.length) {
//     throw new Error('No balance');
//   }

//   const availableBalance = lodash.reduce(listUnspentTxs, (sum, tx) => {
//     return sum + tx.amount;
//   }, 0);

//   if (availableBalance < (amount + fee)) {
//     throw new Error('No enough balance');
//   }

//   const transaction = new jsbgl.Transaction({ version: 2, lockTime: 32451 });

//   lodash.forEach(listUnspentTxs, (tx) => {
//     transaction.addInput({ value: tx.amount * 100000000, txId: tx.txid, vOut: tx.vout });
//   });

//   transaction.addOutput({ value: amount * 100000000, address: to });
//   transaction.addOutput({ value: lodash.floor(availableBalance - amount - fee, 8) * 100000000, address: from });
//   // console.log(transaction)

//   transaction.signInput(0, {
//     privateKey: pk,
//     address: from,
//     sigHashType: 0x00000001
//   });
//   const signedTransaction = transaction.serialize();

//   return await apiSendTransaction(signedTransaction);
// }

// const getSpendableBalance = async (address) => {
//   const listUnspentTxs = await apiGetListUnspentTxs(address);

//   const spendableBalance = lodash.reduce(listUnspentTxs, (sum, tx) => {
//     return sum + tx.amount;
//   }, 0);

//   return lodash.floor(spendableBalance, 8);
// }

// const getLockedBalance = async (address) => {
//   const lockedBalance = await apiGetReceivedBalance(address, 0) - await apiGetReceivedBalance(address, 1);

//   return lodash.floor(lockedBalance, 8);
// }

// const getFullBalance = async (address) => {
//   const spendableBalance = await getSpendableBalance(address)

//   const lockedBalance = await getLockedBalance(address);

//   return lodash.floor(spendableBalance + lockedBalance, 8);
// }

// setTimeout(async () => {
//   await jsbgl.asyncInit();
//   console.log(getWalletByMnemonic(createMnemomic()).getAddress(0));
//   try {
//     // console.log(await getSpendableBalance("bgl1qqfswgzf0jpwglt7twmnn3uut7pvl08h7grrdq0"));
//     // console.log(await getLockedBalance("bgl1qqfswgzf0jpwglt7twmnn3uut7pvl08h7grrdq0"));
//     // console.log(await getFullBalance("bgl1qqfswgzf0jpwglt7twmnn3uut7pvl08h7grrdq0"));

//     // const { result, error } =  await sendBGL("KyNV2RGWajXqVDhSyh4WswDeAjttxdCiQFwopRLkUXRrkPsGkL2S", "bgl1q3dvkqanykqhamde64u4eqj3ut6ffv3qntmpyap", 0.00001);
//     // console.log(result, error)

//     console.log(await apiGetTransaction("c9aea61084a7eeb23c4f7b5bee40ac3de4c1dde951de862882a9b8a8a9f089a1"));
//   } catch (error) {
//     console.log(error.message)
//   }
// }, 10000);


// export default {
//   createMnemomic,
//   getWalletByMnemonic,
//   getBGLAccountFromWallet,
//   sendBGL,
//   getFullBalance,
//   apiGetTransaction
// }