import { AsyncStorage } from 'react-native';
import * as SecureStore from 'expo-secure-store';
import lodash from 'lodash';

export const TOKEN_KEY = 'TOKEN_KEY';
export const WALLETS = 'WALLETS';

export const getStorage = async (name) => {
  try {
    return JSON.parse(await SecureStore.getItemAsync(name) || null)
  } catch (error) {
    return {
      error,
    }
  }
}

export const setStorage = async (name, data) => {
  try {
    await SecureStore.setItemAsync(name, JSON.stringify(data))
    return data
  } catch (error) {
    return {
      error,
    }
  }
}

export const getTransactions = async (address) => {
  try {
    return JSON.parse(await AsyncStorage.getItem(`${address}_transactions`)) || []
  } catch (error) {
    return {
      error,
    }
  }
}

export const addTransaction = async (address, transaction) => {
  try {
    const transactions = await getTransactions(address);
    const index = lodash.findIndex(transactions, { txid: transaction.txid });

    if (index >= 0) {
      transactions[index] = transaction;
    }
    else {
      transactions.push(transaction);
    }

    await AsyncStorage.setItem(`${address}_transactions`, JSON.stringify(transactions))
    return data
  } catch (error) {
    return {
      error,
    }
  }
}