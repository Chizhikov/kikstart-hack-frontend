import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import useColors from '@src/hooks/useColors';
import Constants from 'expo-constants';

const Styles = ({ elementColors }) => StyleSheet.create({
  container: {
    paddingTop: Constants.statusBarHeight,
    height: '100%',
    backgroundColor: elementColors.backgroundScreen
  }
});

const Layout = ({ children, backgroundColor }) => {
  const elementColors = useColors();
  const styles = Styles({ elementColors });

  return (
    <View style={[styles.container, {backgroundColor: backgroundColor}]}>
      {children}
    </View>
  );
}

Layout.propTypes = {
  children: PropTypes.object.isRequired,
  backgroundColor: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string
  ])
};

Layout.defaultProps = {
  backgroundColor: 'rgba(224, 236, 248, 0.5)',
}

export default Layout;