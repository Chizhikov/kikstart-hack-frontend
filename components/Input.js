import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, TextInput, View, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import useColors from '@src/hooks/useColors';
import Fonts from '@src/constants/Fonts';
import Icon from '@src/components/Icon';
import TouchableElement from '@src/components/TouchableElement';

const Styles = ({
  elementColors,
}) => (
  StyleSheet.create({
    container: {
      paddingBottom: 24,
    },
    withErrorContainer: {
      paddingBottom: 0,
    },
    labelText: {
      fontFamily: Fonts.regular,
      fontSize: 16,
      color: elementColors.textAdditional_Blue,
      opacity: 0,
      marginLeft: 3,
      marginBottom: 4,
    },
    labelTextHidden: {
      opacity: 1,
    },

    inputContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: elementColors.backgroundScreen,
      borderColor: elementColors.backgroundScreen,
      borderWidth: 1,
      borderRadius: 3,
      
    },
    clickableInputContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      flex: 1,
      padding: 14,
    },
    clickableIconContainer: {
      flex: 0,
      padding: 14,
    },
    valueText: {
      flex: 1,
      fontFamily: Fonts.regular,
      fontSize: 16,
      color: elementColors.textInput,
    },
    generatedValueText: {
      marginLeft: 5,
      fontFamily: Fonts.regular,
      fontSize: 13,
      color: elementColors.textPlaceholderInput,
    },
    staticText: {
      marginLeft: 5,
      fontFamily: Fonts.regular,
      fontSize: 16,
      color: elementColors.textInput,
    },

    errorContainer: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    errorText: {
      fontFamily: Fonts.regular,
      fontSize: 12,
      color: elementColors.textDecline,
    }
  })
);

const Input = ({
  label,
  value,
  placeholder,
  onChangeText,
  textAlign,
  isSecured,
  generatedValue,
  staticText,
  errorText,
  clickableIcon,
  inputProps
}) => {
  const elementColors = useColors();
  const [styles, updateStyles] = useState(Styles({
    elementColors
  }));

  useEffect(() => {
    updateStyles(Styles({ elementColors }));
  }, [elementColors]);

  const inputRef = useRef(null);

  return (
      <View style={[styles.container, !!errorText ? styles.withErrorContainer : {}]}>
        {!!label && (<Text style={[styles.labelText, !!(value?.length) && styles.labelTextHidden]}>{ label }</Text>)}
        <View style={[styles.inputContainer]}>
          <TouchableWithoutFeedback onPress={() => {inputRef.current.focus()}}>
            <View style={[styles.clickableInputContainer]}>
              <TextInput
                ref={inputRef}
                style={[styles.valueText]}
                value={value}
                onChangeText={onChangeText}
                placeholder={placeholder}
                placeholderTextColor={elementColors.textPlaceholderInput}
                secureTextEntry={isSecured}
                textAlign={textAlign}
                { ...inputProps }
              />
              {!!generatedValue && <Text style={[styles.generatedValueText]}>{generatedValue}</Text>}
              {!!staticText && <Text style={[styles.staticText]}>{staticText}</Text>}
            </View>
          </TouchableWithoutFeedback>
          {!!clickableIcon && (
            <TouchableElement style={[styles.clickableIconContainer]} onPress={clickableIcon.onPress}>
              <Icon
                icon={clickableIcon.icon}
                height={24}
                width={24}
                color={elementColors.textInput}
              />
            </TouchableElement>
          )}
        </View>
        {!!errorText && (
          <View style={[styles.errorContainer]}>
            {/* <Icon
              icon={require('@src/assets/images/home.png')}
              height={24}
              width={24}
              color={elementColors.textDecline}
            /> */}
            <Text style={[styles.errorText]}>{errorText}</Text>
          </View>
        )}
      </View>
  );
}

Input.propTypes = {
  isSecured: PropTypes.bool,
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string,
  textAlign: PropTypes.string,
  generatedValue: PropTypes.string,
  staticText: PropTypes.string,
  errorText: PropTypes.string,
  clickableIcon: PropTypes.shape({
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.number.isRequired
  }),
  inputProps: PropTypes.object,
  label: PropTypes.string,
};

Input.defaultProps = {
  value: '',
  textAlign: 'left',
  isSecured: false,
  placeholder: '',
  generatedValue: '',
  staticText: '',
  errorText: '',
  clickableIcon: null,
  inputProps: {},
  label: null,
};

export default Input;