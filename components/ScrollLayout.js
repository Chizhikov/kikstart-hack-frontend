import React, { useState, createContext } from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native-gesture-handler';

export const ScrollScreenContext = createContext();

const ScrollLayout = ({ children, scrolled }) => {
  const [scrollEnabled, setScrollEnabled] = useState(scrolled);
  const [disableScrollViewPanResponder, setDisableScrollViewPanResponder] = useState(true);

  return (
    <ScrollScreenContext.Provider value={{ setScrollEnabled, setDisableScrollViewPanResponder }}>
      <ScrollView scrollEnabled={scrollEnabled} disableScrollViewPanResponder={disableScrollViewPanResponder}>
        {children}
      </ScrollView>
    </ScrollScreenContext.Provider>
  );
}

ScrollLayout.propTypes = {
  children: PropTypes.node.isRequired,
  scrolled: PropTypes.bool
};

ScrollLayout.defaultProps = {
  scrolled: true
};

export default ScrollLayout;