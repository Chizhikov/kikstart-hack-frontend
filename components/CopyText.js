import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Clipboard } from 'react-native';
import PropTypes from 'prop-types';
import useColors from '@src/hooks/useColors';
import Icon from '@src/components/Icon';
import TouchableElement from '@src/components/TouchableElement';

const Styles = ({
  elementColors
}) => (
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: elementColors.backgroundCopy,
      borderColor: elementColors.borderCopy,
      borderWidth: 1,
      borderRadius: 3,
      padding: 14,
    },
    text: {
      flex: 1,
      paddingRight: 10,
      fontSize: 16,
      color: elementColors.text,
    },
    textHide: {
      opacity: 0,
    },
    textCopied: {
      position: 'absolute',
      width: '100%',
      textAlign: 'center',
      justifyContent: 'center',
      fontSize: 16,
      color: elementColors.text,
    },
  })
);

const CopyText = ({ text }) => {
  const elementColors = useColors();
  const [styles, updateStyles] = useState(Styles({
    elementColors
  }));

  useEffect(() => {
    updateStyles(Styles({ elementColors }));
  }, [elementColors]);

  const [copied, setCopied] = useState(false);

  const copyText = () => {
    Clipboard.setString(text);
    setCopied(true);
  }

  useEffect(() => {
    const timer = setTimeout(() => {
      if (copied)
        setCopied(false);
    }, 2000);
    
    return () => {
      if (timer)
        clearTimeout(timer);
    }
  }, [copied])

  return (
    <TouchableElement onPress={copyText}>
      <View style={styles.container}>
        <Text style={[
          styles.text,
          !!copied && styles.textHide,
        ]}>{text}</Text>
        {copied && (
          <Text style={styles.textCopied}>Copied</Text>
        )}
        <View>
          <Icon
            icon={require('@src/assets/images/copy-black.png')}
            width={20}
            height={23}
            color={elementColors.textInput}
          />
        </View>
      </View>
    </TouchableElement>
  );
}

CopyText.propTypes = {
  text: PropTypes.string.isRequired,
};

CopyText.defaultProps = {};

export default CopyText;