import React, { useState, useEffect } from 'react';
import {
  Text, 
  StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';
import useColors from '@src/hooks/useColors';
import TouchableElement from '@src/components/TouchableElement';
import Fonts from '@src/constants/Fonts';

const Styles = ({
  elementColors,
}) => {
  const container = {};
  const text = {};
  text.fontFamily = Fonts.regular;
  container.borderRadius = 4;
  container.borderWidth = 1.22;
  container.borderColor = elementColors.backgroundButtonApply;
  container.backgroundColor = elementColors.backgroundButton;
  text.color = elementColors.textButton;
  
  return StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',

      margin: 0,
      marginTop: 0,
      marginBottom: 0,
      marginLeft: 0,
      marginRight: 0,
      paddingTop: 8,
      paddingBottom: 8,
      paddingLeft: 10.5,
      paddingRight: 10.5,

      ...container,
    },
    text: {
      textAlign: 'center',
      marginTop: 0,
      fontSize: 18, 

      ...text,
    },
    shadow: {
      shadowColor: elementColors.borderShadow,
      shadowOffset: {
        width: 2,
        height: 2,
      },
      shadowOpacity: 0.1,
      shadowRadius: 3.84,
      elevation: 3,
    }
  })
};

const Button = ({
  hasShadow,
  text,
  onPress,
  containerStyle,
  textStyle,
  textProps
}) => {
  const elementColors = useColors();
  const [styles, updateStyles] = useState(Styles({ elementColors }));

  useEffect(() => {
    updateStyles(Styles({ elementColors }));
  }, [elementColors]);

  return (
    <TouchableElement
      style={[
        styles.container,
        hasShadow ? styles.shadow : null,
        containerStyle
      ]}
      onPress={onPress}
    >
      {!!text && <Text
        style={[
          styles.text,
          textStyle,
        ]}
        {...textProps}>
        {text}
      </Text>}
    </TouchableElement>
  );
}

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  hasShadow: PropTypes.bool,
  text: PropTypes.string,
  containerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  textStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  textProps: PropTypes.object
};

Button.defaultProps = {
  hasShadow: false,
  text: '',
  containerStyle: {},
  textStyle: {},
  textProps: {}
};

export default Button;