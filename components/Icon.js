import * as React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';

const Icon = ({ color, width, height, icon }) => {
  const style = {
    tintColor: color
  }

  if (width)
    style.width = width;

  if (height)
    style.height = height;

  return (
    <Image
      resizeMode="contain"
      tintColor={color}
      source={icon}
      style={style}
    />
  );
}

Icon.propTypes = {
  icon: PropTypes.number.isRequired,
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Icon.defaultProps = {
  width: 0,
  height: 0,
  color: undefined,
};

export default Icon;