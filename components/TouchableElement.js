import React from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

const TouchableElement = ({ style, children, onPress }) => {
  return (
    <TouchableOpacity 
      style={style}
      onPress={onPress}
    >
      {children}
    </TouchableOpacity>
  );
}

TouchableElement.propTypes = {
  children: PropTypes.node.isRequired,
  onPress: PropTypes.func.isRequired,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ])
};

TouchableElement.defaultProps = {
  style: {},
};

export default TouchableElement;