import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { useNavigation, useNavigationState } from '@react-navigation/native';

import Button from '@src/components/Button';
import Icon from '@src/components/Icon';
import TouchableElement from './TouchableElement';


const Styles = () => (
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      // backgroundColor: 'rgba(224, 236, 248, 0.5)',
      marginBottom: 20,
    },
    backButtonContainer: {
      flex: 1,
      paddingHorizontal: 17,
      paddingVertical: 10,
      flexDirection: 'row',
      alignItems: 'center',
      // backgroundColor: 'yellow',
    },
    textButton: {
      fontSize: 16,
    },
    button: {
      margin: 0,
      padding: 0,
      borderWidth: 0,
      // borderColor: 'rgba(224, 236, 248, 0.5)',
    }
  }
));

const NavigationBar = ({ hasBackButton, colorNone }) => {
  const [styles] = useState(Styles());

  const navigation = useNavigation();
  const navigationHistory = useNavigationState(({ history, routes, type }) => {
    if (type === 'tab')
      return history && history.map((item) => routes.find((route) => item.key === route.key));
    else if (type === 'stack')
      return routes;
    else
      return [];
  });

  const previousPageName = navigationHistory && navigationHistory.length >= 2 ? 
    navigationHistory[navigationHistory.length - 2].name : '';

  return (
    <View style={[styles.container, {backgroundColor: !colorNone ? 'rgba(224, 236, 248, 0.5)': ''}]}>
      {!!hasBackButton && navigation.canGoBack() && 
        <TouchableElement style={styles.backButtonContainer} onPress={() => navigation.goBack()}>
          <Icon 
            icon={require('@src/assets/images/back.png')}
            width={23}
            height={23}
          />
          {/* <Button
            // text={previousPageName}
            containerStyle={styles.button}
            textStyle={styles.textButton}
            onPress={() => {}}
          /> */}
        </TouchableElement>
      }
    </View>

  );
};

NavigationBar.propTypes = {
  hasBackButton: PropTypes.bool,
  colorNone: PropTypes.bool,
};

NavigationBar.defaultProps = {
  hasBackButton: true,
  colorNone: false
};

export default NavigationBar;